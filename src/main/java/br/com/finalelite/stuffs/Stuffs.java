package br.com.finalelite.stuffs;

import br.com.finalelite.stuffs.listeners.BlockListener;
import br.com.finalelite.stuffs.listeners.PlayerListener;
import org.bukkit.plugin.java.JavaPlugin;

public class Stuffs {

    public Stuffs(JavaPlugin plugin) {
        plugin.getServer().getPluginManager().registerEvents(new PlayerListener(), plugin);
        plugin.getServer().getPluginManager().registerEvents(new BlockListener(), plugin);
    }

}
