package br.com.finalelite.stuffs.listeners;

import br.com.finalelite.kits.Main;
import lombok.val;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

public class PlayerListener implements Listener {

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event) {
        val player = event.getEntity();

        val vip = Main.getDb().getVIP(player);
        if (vip == null)
            return;

        event.setKeepLevel(true);
        event.setDroppedExp(0);
    }

}

