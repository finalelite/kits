package br.com.finalelite.stuffs.listeners;

import br.com.finalelite.kits.Main;
import lombok.val;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Arrays;
import java.util.List;

public class BlockListener implements Listener {

    public static List<Material> noFall = Arrays.asList(Material.SAND, Material.RED_SAND, Material.GRAVEL, Material.ANVIL, Material.DAMAGED_ANVIL, Material.CHIPPED_ANVIL);

    @EventHandler
    public void onBlockTurnsToEntity(EntityChangeBlockEvent event) {
        if (event.getEntityType() != EntityType.FALLING_BLOCK)
            return;
        val block = (FallingBlock) event.getEntity();
        if (!block.getLocation().getWorld().getName().equalsIgnoreCase("Trappist-1b"))
            return;
        if (event.getTo() == Material.AIR && event.getEntityType() == EntityType.FALLING_BLOCK) {
            val material = block.getBlockData().getMaterial();
            if (noFall.contains(material)) {
                event.setCancelled(true);
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        block.getLocation().getNearbyEntities(30, 30, 30).stream()
                                .filter(entity -> entity.getType() == EntityType.PLAYER)
                                .forEach(entity -> {
                                    val player = ((Player) entity);
                                    player.sendBlockChange(block.getLocation(), block.getBlockData());
                                });
                    }
                }.runTaskLater(Main.getInstance(), 2);

            }
        }

    }


}
