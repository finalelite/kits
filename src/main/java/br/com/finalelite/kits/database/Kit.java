package br.com.finalelite.kits.database;

import lombok.Data;
import org.bukkit.inventory.ItemStack;

import java.util.List;

@Data
public class Kit {
    private String name;
    private String displayName;
    private List<ItemStack> items;
}
