package br.com.finalelite.kits.database;

import lombok.Getter;

public enum KitTime {
    TEN_MINUTES(600),
    SIX_PLAYED_HOURS(3600 * 6),
    DAILY(86400),
    TWENTY_DAYS(86400 * 20),
    WEEKLY(86400 * 7),
    MONTHLY(86400 * 30);

    @Getter
    private int seconds;

    KitTime(int seconds) {
        this.seconds = seconds;
    }

    public static KitTime valueOf(byte b) {
        return KitTime.values()[b];
    }

    public boolean isPlayed() {
        return this == SIX_PLAYED_HOURS;
    }
}
