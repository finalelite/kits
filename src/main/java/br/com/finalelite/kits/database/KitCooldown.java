package br.com.finalelite.kits.database;

import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.Statistic;

import java.util.Date;
import java.util.UUID;

public class KitCooldown {

    private final String uuid;
    private final KitType type;
    private final KitTime time;
    private final long lastUse;
    private final boolean isNow;

    public KitCooldown(String uuid, KitType type, KitTime time, long lastUse, boolean isNow) {
        this.uuid = uuid;
        this.type = type;
        this.time = time;
        this.lastUse = lastUse;
        this.isNow = isNow;
    }

    public KitCooldown(String uuid, KitType type, KitTime time, long lastUse) {
        this(uuid, type, time, lastUse, false);
    }

    public KitCooldown(String uuid, byte type, byte time, long lastUse) {
        this(uuid, KitType.valueOf(type), KitTime.valueOf(time), lastUse);
    }

    public boolean canUse() {
        if (lastUse == -1)
            return true;
        if (isNow)
            return true;
        if (time.isPlayed())
            return (now() - lastUse) / 20 >= time.getSeconds();
        return (now() - lastUse) / 1000 >= time.getSeconds();

    }

    public int getReamingSeconds() {
        if (canUse())
            return 0;
        if (time.isPlayed())
            return (int) (time.getSeconds() - ((now() - lastUse) / 20));
        return (int) (time.getSeconds() - ((now() - lastUse) / 1000));
    }

    public String formatReamingTime() {
        val secs = getReamingSeconds();

        val days = secs / 86400;
        val hours = (secs % 86400) / 3600;
        val minutes = (secs % 3600) / 60;
        val seconds = secs % 60;

        StringBuilder sb = new StringBuilder();
        if (days >= 1) {
            sb.append(days);
            sb.append(days == 1 ? " dia " : " dias ");
        }
        if (hours >= 1) {
            sb.append(hours);
            sb.append(hours == 1 ? " hora " : " horas ");
        }
        if (minutes >= 1) {
            sb.append(minutes);
            sb.append(minutes == 1 ? " minuto " : " minutos ");
        }
        if (seconds >= 1) {
            sb.append("e ");
            sb.append(seconds);
            sb.append(seconds == 1 ? " segundo " : " segundos ");
        }
        return sb.toString().trim();
    }
    
    public boolean isPlayed() {
        return time.isPlayed();
    }

    private long now() {
        if (!time.isPlayed()) {
            return new Date().getTime();
        }
        return Bukkit.getPlayer(UUID.fromString(uuid)).getStatistic(Statistic.PLAY_ONE_MINUTE);
    }

}
