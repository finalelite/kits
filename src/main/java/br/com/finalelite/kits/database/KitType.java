package br.com.finalelite.kits.database;

public enum KitType {
    BASIC,
    GUILD_ANCIA_1,
    GUILD_ANCIA_2,
    GUILD_ANCIA_3,
    GUILD_ANCIA_4,
    GUILD_ANCIA_5,
    GUILD_NOBRE_1,
    GUILD_NOBRE_2,
    GUILD_NOBRE_3,
    GUILD_NOBRE_4,
    GUILD_NOBRE_5,
    GUILD_SANGUINARIA_1,
    GUILD_SANGUINARIA_2,
    GUILD_SANGUINARIA_3,
    GUILD_SANGUINARIA_4,
    GUILD_SANGUINARIA_5,
    VIP_CONDE,
    VIP_LORD,
    VIP_DUQUE,
    VIP_TITAN,
    SPECIAL;

    public static KitType valueOf(byte b) {
        return KitType.values()[b];
    }

    public boolean isVIP() {
        return this.name().contains("VIP");
    }

}
