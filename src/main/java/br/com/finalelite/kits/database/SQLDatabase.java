package br.com.finalelite.kits.database;

import com.gitlab.pauloo27.core.sql.*;
import lombok.val;
import lombok.var;
import org.bukkit.Statistic;
import org.bukkit.entity.Player;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class SQLDatabase {

    private EzSQL sql;
    private EzTable cooldowns;
    private EzTable history;

    public SQLDatabase(String address, int port, String username, String password) {
        // TODO remove tries from constructor :P
        sql = new EzSQL(EzSQLType.MYSQL);
        sql.withAddress(address, port);
        sql.withCustomDriver("com.mysql.jdbc.Driver");
        sql.withDefaultDatabase("kits", true);
        if (password == null)
            sql.withLogin(username);
        else
            sql.withLogin(username, password);
        try {
            sql.connect();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        createTable();
    }

    private void createTable() {
        try {

            cooldowns = sql.createIfNotExists(
                    new EzTableBuilder("cooldowns")
                            .withColumn(new EzColumnBuilder("uuid", EzDataType.VARCHAR, 36, EzAttribute.NOT_NULL))
                            .withColumn(new EzColumnBuilder("type", EzDataType.TINYINT, EzAttribute.NOT_NULL))
                            .withColumn(new EzColumnBuilder("time", EzDataType.TINYINT, EzAttribute.NOT_NULL))
                            .withColumn(new EzColumnBuilder("lastUse", EzDataType.BIGINT, EzAttribute.NOT_NULL))
            );
        } catch (SQLException e) {
            e.printStackTrace();
        }

        history = sql.getTable("Coins_history");
    }

    public KitCooldown getCooldownAndSetToNow(Player player, KitType type, KitTime time) {
        try (val result = cooldowns.select(new EzSelect("lastUse")
                .where().equals("uuid", player.getUniqueId().toString())
                .and().equals("type", type.ordinal())
                .and().equals("time", time.ordinal()))) {
            val rs = result.getResultSet();
            if (rs.next()) {
                val lastUse = rs.getLong("lastUse");
                val uuid = player.getUniqueId().toString();
                var cd = new KitCooldown(uuid, type, time, lastUse);
                if (cd.canUse()) {
                    var now = new Date().getTime();
                    if (time.isPlayed())
                        now = player.getStatistic(Statistic.PLAY_ONE_MINUTE);

                    cooldowns.update(new EzUpdate().set("lastUse", now)
                            .where().equals("uuid", uuid)
                            .and().equals("type", type.ordinal())
                            .and().equals("time", time.ordinal())).close();
                    cd = new KitCooldown(uuid, type, time, now, true);
                }
                return cd;
            } else {
                val uuid = player.getUniqueId().toString();
                var now = new Date().getTime();
                if (time.isPlayed())
                    now = player.getStatistic(Statistic.PLAY_ONE_MINUTE);
                cooldowns.insert(new EzInsert("uuid, type, time, lastUse",
                        uuid, type.ordinal(), time.ordinal(), now)).close();
                return new KitCooldown(uuid, type, time, -1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void truncate() {
        try {
            cooldowns.truncate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getTag(String UUID) {
        try {
            PreparedStatement st = sql.getConnection().prepareStatement("SELECT Tag FROM hubfinal.Tag_data WHERE UUID = ?;");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("Tag");
            }
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public PlayerVIP getVIP(Player player) {
        return PlayerVIP.fromValue(getTag(player.getUniqueId().toString()));
    }

    public PlayerGuild getGuild(Player player) {
        return PlayerGuild.fromValue(getGuild(player.getUniqueId().toString()) + "_" + getGuildLevel(player.getUniqueId().toString()));
    }

    public boolean canGetResetKit(Player player) {
        try (val result = history.select(new EzSelect("Coins, Blackcoins")
                .where().equals("UUID", player.getUniqueId().toString())
                .and()
                .openParentheses()
                .atLeast("Coins", 1000)
                .or().atLeast("Blackcoins", 500)
                .closeParentheses()).
                getResultSet()) {
            return result.next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void removeResetKit(Player player) {
        try {
            history.delete(new EzDelete()
                    .where().equals("UUID", player.getUniqueId().toString())).close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getGuild(String UUID) {
        try {
            PreparedStatement st = sql.getConnection().prepareStatement("SELECT Guilda FROM mixedup.Guilda_data WHERE UUID = ?;");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getString("Guilda");
            }
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public int getGuildLevel(String UUID) {
        try {
            PreparedStatement st = sql.getConnection().prepareStatement("SELECT Rank FROM mixedup.Guilda_data WHERE UUID = ?;");
            st.setString(1, UUID);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("Rank");
            }
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public void setTag(Player player, String tag) {
        val uuid = player.getUniqueId().toString();
        try {
            PreparedStatement st = sql.getConnection().prepareStatement("UPDATE hubfinal.Tag_data SET Tag = ? WHERE UUID = ?;");
            st.setString(1, tag);
            st.setString(2, uuid);
            st.executeUpdate();
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void setGuild(Player player, String guildName, int guildLevel) {
        val uuid = player.getUniqueId().toString();
        try {
            PreparedStatement st = sql.getConnection().prepareStatement("UPDATE mixedup.Guilda_data SET Guilda = ? Rank = ? WHERE UUID = ?;");
            st.setString(1, guildName);
            st.setInt(2, guildLevel);
            st.setString(3, uuid);
            st.executeUpdate();
            st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
