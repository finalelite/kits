package br.com.finalelite.kits.database;

import lombok.Getter;

import java.util.Arrays;

public enum PlayerVIP {
    CONDE("vip"),
    LORD("vip+"),
    DUQUE("vip++"),
    TITAN("vip+++");

    @Getter
    private String value;

    PlayerVIP(String value) {
        this.value = value;
    }

    public static PlayerVIP fromValue(String string) {
        if (string == null)
            return null;
        return Arrays.stream(PlayerVIP.values()).filter(vip -> vip.getValue().equalsIgnoreCase(string)).findFirst().orElse(null);
    }

    public static PlayerVIP fromName(String string) {
        if (string == null)
            return null;
        return Arrays.stream(PlayerVIP.values()).filter(vip -> vip.name().equalsIgnoreCase(string)).findFirst().orElse(null);
    }

}
