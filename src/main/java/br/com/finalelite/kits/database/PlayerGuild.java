package br.com.finalelite.kits.database;

import lombok.Getter;

import java.util.Arrays;

public enum PlayerGuild {
    SANGUINARIA_1("sanguinaria_1"),
    SANGUINARIA_2("sanguinaria_2"),
    SANGUINARIA_3("sanguinaria_3"),
    SANGUINARIA_4("sanguinaria_4"),
    SANGUINARIA_5("sanguinaria_5"),
    ANCIA_1("ancia_1"),
    ANCIA_2("ancia_2"),
    ANCIA_3("ancia_3"),
    ANCIA_4("ancia_4"),
    ANCIA_5("ancia_5"),
    NOBRE_1("nobre_1"),
    NOBRE_2("nobre_2"),
    NOBRE_3("nobre_3"),
    NOBRE_4("nobre_4"),
    NOBRE_5("nobre_5");

    @Getter
    private String value;

    PlayerGuild(String value) {
        this.value = value;
    }

    public static PlayerGuild fromValue(String string) {
        if (string == null)
            return null;
        return Arrays.stream(PlayerGuild.values()).filter(guild -> guild.getValue().equalsIgnoreCase(string)).findFirst().orElse(null);
    }

    public static PlayerGuild fromName(String string) {
        if (string == null)
            return null;
        return Arrays.stream(PlayerGuild.values()).filter(guild -> guild.name().equalsIgnoreCase(string)).findFirst().orElse(null);
    }

}
