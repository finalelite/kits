package br.com.finalelite.kits;

import br.com.finalelite.kits.commands.*;
import br.com.finalelite.kits.database.SQLDatabase;
import br.com.finalelite.pauloo27.api.PauloAPI;
import br.com.finalelite.pauloo27.api.commands.CommandablePlugin;
import br.com.finalelite.pauloo27.api.gui.GUIManager;
import br.com.finalelite.stuffs.Stuffs;
import lombok.Getter;
import lombok.val;

import java.io.File;

public class Main extends CommandablePlugin {

    @Getter
    private static Main instance;
    @Getter
    private static SQLDatabase db;
    @Getter
    private static GUIManager gui;

    @Override
    public void onEnable() {
        instance = this;

        registerCommand(new FEKitAdd());
        registerCommand(new FEKitDel());
        registerCommand(new FETag());
        registerCommand(new FEGuild());
        registerCommand(new FEEnchantCommand());
        registerCommand(new FENameCommand());
        registerCommand(new FELoreCommand());
        registerCommand(new KitsCommand());

        val configFile = new File("config.yml");
        if (!configFile.exists())
            saveDefaultConfig();

        val info = PauloAPI.getInstance().getDatabaseInformation();
        val address = info.getAddress();
        val port = info.getPort();
        val username = info.getUsername();
        val password = info.getPassword();

        db = new SQLDatabase(address, port, username, password);
        gui = new GUIManager(this);

        // stuffs
        new Stuffs(this);
    }

}
