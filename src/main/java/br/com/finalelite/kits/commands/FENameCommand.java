package br.com.finalelite.kits.commands;


import br.com.finalelite.pauloo27.api.commands.BaseCommand;
import br.com.finalelite.pauloo27.api.commands.usage.DefaultParameterType;
import br.com.finalelite.pauloo27.api.commands.usage.Parameter;
import lombok.val;
import org.bukkit.ChatColor;

public class FENameCommand extends BaseCommand {

    public FENameCommand() {
        super("fename");

        setPermissionMessage("&cSem permissão.");
        setPermission("enchant.admin");

        usage.addParameter(new Parameter("nome", DefaultParameterType.STRING, true));

        playerListener = (cmd) -> {
            val name = cmd.getRawArgumentsBeforeAndIncluding(0);
            val player = cmd.getSender();
            val item = player.getItemInHand();
            val meta = item.getItemMeta();

            if (!player.isOp())
                return false;

            if (name.equalsIgnoreCase("clear"))
                meta.setDisplayName(null);
            else
                meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
            item.setItemMeta(meta);

            player.setItemInHand(item);

            return true;
        };
    }
}
