package br.com.finalelite.kits.commands;


import br.com.finalelite.kits.Main;
import br.com.finalelite.pauloo27.api.commands.BaseCommand;
import br.com.finalelite.pauloo27.api.commands.usage.DefaultParameterType;
import br.com.finalelite.pauloo27.api.commands.usage.Parameter;
import lombok.val;

public class FEKitDel extends BaseCommand {

    public FEKitDel() {
        super("fekitdel");

        setPermissionMessage("&cSem permissão.");
        setPermission("enchant.admin");

        usage.addParameter(new Parameter("padrao/guilda/vip_conde/vip_lord/vip_duque/viptitan", DefaultParameterType.STRING, true)); // 0


        playerListener = (cmd) -> {
            val rawName = cmd.getArgument(0).toString().toLowerCase();
            val player = cmd.getSender();

            if (!player.isOp())
                return false;

            Main.getInstance().getConfig().set("Kits." + rawName, null);
            Main.getInstance().saveConfig();
            Main.getInstance().reloadConfig();

            cmd.reply("&aKit " + rawName + " &aapagado com sucesso!");
            return true;
        };
    }
}
