package br.com.finalelite.kits.commands;


import br.com.finalelite.kits.Main;
import br.com.finalelite.pauloo27.api.commands.BaseCommand;
import br.com.finalelite.pauloo27.api.commands.usage.DefaultParameterType;
import br.com.finalelite.pauloo27.api.commands.usage.Parameter;
import lombok.val;
import org.bukkit.entity.Player;

public class FETag extends BaseCommand {

    public FETag() {
        super("fetag");

        setPermissionMessage("&cSem permissão.");
        setPermission("adm.admin.admir");

        usage.addParameter(new Parameter("jogador", DefaultParameterType.PLAYER, true)); // 0
        usage.addParameter(new Parameter("tag", DefaultParameterType.STRING, true)); // 1


        genericListener = (cmd) -> {
            val player = cmd.getSender();
            if (!player.isOp())
                return false;
            Main.getDb().setTag((Player) cmd.getArgument(0), (String) cmd.getArgument(1));
            cmd.reply("&aTag alterada.");
            return true;
        };
    }
}
