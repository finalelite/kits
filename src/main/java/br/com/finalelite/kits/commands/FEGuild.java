package br.com.finalelite.kits.commands;


import br.com.finalelite.kits.Main;
import br.com.finalelite.pauloo27.api.commands.BaseCommand;
import br.com.finalelite.pauloo27.api.commands.usage.DefaultParameterType;
import br.com.finalelite.pauloo27.api.commands.usage.Parameter;
import lombok.val;
import org.bukkit.entity.Player;

public class FEGuild extends BaseCommand {

    public FEGuild() {
        super("feguild");

        setPermissionMessage("&cSem permissão.");
        setPermission("adm.admin.admir");

        usage.addParameter(new Parameter("jogador", DefaultParameterType.PLAYER, true)); // 0
        usage.addParameter(new Parameter("guilda", DefaultParameterType.STRING, true)); // 1
        usage.addParameter(new Parameter("nivel", DefaultParameterType.INTEGER, true)); // 2


        genericListener = (cmd) -> {
            val player = cmd.getSender();
            if (!player.isOp())
                return false;
            Main.getDb().setGuild((Player) cmd.getArgument(0), (String) cmd.getArgument(1), (int) cmd.getArgument(2));
            cmd.reply("&aTag alterada.");
            return true;
        };
    }
}
