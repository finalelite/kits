package br.com.finalelite.kits.commands;

import br.com.finalelite.kits.Main;
import br.com.finalelite.kits.database.KitTime;
import br.com.finalelite.kits.database.KitType;
import br.com.finalelite.kits.gui.MainMenu;
import br.com.finalelite.pauloo27.api.PauloAPI;
import br.com.finalelite.pauloo27.api.commands.BaseCommand;
import br.com.finalelite.pauloo27.api.commands.usage.DefaultParameterType;
import br.com.finalelite.pauloo27.api.commands.usage.Parameter;
import lombok.val;
import org.bukkit.inventory.ItemStack;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class KitsCommand extends BaseCommand {

    public KitsCommand() {
        super("kits");
        setAliases(Collections.singletonList("kit"));

        usage.addParameter(new Parameter("nome do kit", DefaultParameterType.STRING, false));
        usage.addParameter(new Parameter("jogador", DefaultParameterType.PLAYER, false, "kits.admin.others"));

        playerListener = (cmd) -> {
            val player = cmd.getSender();
            if (!cmd.hasArgument(0)) {
                Main.getGui().open(player, new MainMenu());
                return true;
            }

            if (cmd.hasArgument(1)) {
                cmd.reply("&cAinda não é possivel dar kits para outros jogadores.");
                return true;
            }

            val kitName = ((String) cmd.getArgument(0)).toLowerCase();

            if (kitName.equalsIgnoreCase("truncate")) {
                if (!player.isOp())
                    return false;

                Main.getDb().truncate();
                cmd.reply("&aTabela limpa.");
                return true;
            } else if (kitName.equalsIgnoreCase("ver")) {
                //Main.getGui().open(player, new KitPreview("comida"));
                return true;
            }

            val config = Main.getInstance().getConfig();

            if (config.getString("Kits." + kitName + ".Name") != null) {
                if (kitName.equals("reset")) {
                    cmd.reply("&cVocê não pode pegar esse kit.");
                    return true;
                } else if (kitName.startsWith("vip_")) {
                    val vipName = kitName.substring("vip_".length(), kitName.lastIndexOf("_"));
                    val playerVIP = Main.getDb().getVIP(player);
                    if (!PauloAPI.getInstance().getAccountInfo(player).getRole().isMajorStaff() &&
                            !player.isOp() && (Objects.isNull(playerVIP) || !playerVIP.name().equalsIgnoreCase(vipName)) &&
                            !(PauloAPI.getInstance().getAccountInfo(player).getRole().isStaff() && vipName.equalsIgnoreCase("duque"))) {
                        cmd.reply("&cVocê não tem permissão.");
                        return true;
                    }
                } else if (kitName.startsWith("guilda_")) {
                    val guildName = kitName.substring("guilda_".length(), kitName.lastIndexOf("_"));
                    val playerGuild = Main.getDb().getGuild(player);
                    if (!PauloAPI.getInstance().getAccountInfo(player).getRole().isMajorStaff()
                            &&
                            (playerGuild == null || !playerGuild.name().equalsIgnoreCase(guildName)) && !player.isOp()) {
                        cmd.reply("&cVocê não tem permissão.");
                        return true;
                    }
                } else if (!kitName.startsWith("basico_")) {
                    if (!player.hasPermission("kits.collect." + kitName)) {
                        cmd.reply("&cVocê não tem permissão.");
                        return true;
                    }
                }

                val type = config.getString("Kits." + kitName + ".Type");
                val time = config.getString("Kits." + kitName + ".Time");
                val items = getKitItems(kitName);

                val emptySlots = Arrays.stream(player.getInventory().getStorageContents()).filter(Objects::isNull).count();
                if (items.size() > emptySlots) {
                    cmd.reply(String.format("&cVocê precisa deixar, ao menos, mais %d %s no seu inventário para pegar esse kit.",
                            items.size() - emptySlots, (items.size() - emptySlots) == 1 ? "espaço livre" : "espaços livres"));
                    return true;
                }

                val cd = Main.getDb().getCooldownAndSetToNow(player, KitType.valueOf(type.toUpperCase()), KitTime.valueOf(time.toUpperCase()));
                if (!cd.canUse()) {
                    cmd.reply("&aVocê precisa esperar mais " + cd.formatReamingTime()
                            + " para coletar esse kit novamente.");
                    
                    if(cd.isPlayed())
                        cmd.reply("&aLembre-se, esse kit usa o seu tempo online para definir o " +
                        "tempo de espera, logo, você pode pega-lo a cada 6 horas jogadas.");

                    if (!PauloAPI.getInstance().getAccountInfo(player).getRole().isMajorStaff()) {
                        return true;
                    } else {
                        cmd.reply("&aStaff bypass, você pode pegar sim!");
                    }
                }

                val displayName = config.getString("Kits." + kitName + ".Name");
//                Arrays.stream(player.getInventory().getContents()).forEach(i -> player.sendMessage(i == null ? "I NULL" : i.getType() == null ? "NULL" : i.getType().name()));

                items.forEach(player.getInventory()::addItem);

                cmd.reply("&bKit &f" + displayName + "&b coletado.");
                if (kitName.equalsIgnoreCase("reset"))
                    Main.getDb().removeResetKit(player);
            } else {
                cmd.reply("&cKit inválido");
                return false;
            }

            return true;
        };

    }

    public static List<ItemStack> getKitItems(String kitName) {
        return (List<org.bukkit.inventory.ItemStack>) Main.getInstance().getConfig().getList("Kits." + kitName.toLowerCase() + ".Items");
    }

    public static String getDisplayName(String kitName) {
        return Main.getInstance().getConfig().getString("Kits." + kitName.toLowerCase() + ".Name");
    }
}
