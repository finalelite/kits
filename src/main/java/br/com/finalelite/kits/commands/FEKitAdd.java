package br.com.finalelite.kits.commands;


import br.com.finalelite.kits.Main;
import br.com.finalelite.kits.database.KitTime;
import br.com.finalelite.kits.database.KitType;
import br.com.finalelite.kits.gui.KitPreview;
import br.com.finalelite.kits.gui.MainMenu;
import br.com.finalelite.pauloo27.api.commands.BaseCommand;
import br.com.finalelite.pauloo27.api.commands.usage.DefaultParameterType;
import br.com.finalelite.pauloo27.api.commands.usage.Parameter;
import br.com.finalelite.pauloo27.api.message.MessageUtils;
import lombok.val;
import lombok.var;
import org.bukkit.ChatColor;
import org.bukkit.Material;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class FEKitAdd extends BaseCommand {

    private static final Map<String, KitType> types = new HashMap<>();
    private static final Map<String, KitTime> times = new HashMap<>();
    private static final Map<String, String> names = new HashMap<>();

    static {
        types.put("basico", KitType.BASIC);
        types.put("vip_conde", KitType.VIP_CONDE);
        types.put("vip_lord", KitType.VIP_LORD);
        types.put("vip_duque", KitType.VIP_DUQUE);
        types.put("vip_titan", KitType.VIP_TITAN);
        types.put("guilda_ancia_1", KitType.GUILD_ANCIA_1);
        types.put("guilda_ancia_2", KitType.GUILD_ANCIA_2);
        types.put("guilda_ancia_3", KitType.GUILD_ANCIA_3);
        types.put("guilda_ancia_4", KitType.GUILD_ANCIA_4);
        types.put("guilda_ancia_5", KitType.GUILD_ANCIA_5);
        types.put("guilda_sanguinaria_1", KitType.GUILD_SANGUINARIA_1);
        types.put("guilda_sanguinaria_2", KitType.GUILD_SANGUINARIA_2);
        types.put("guilda_sanguinaria_3", KitType.GUILD_SANGUINARIA_3);
        types.put("guilda_sanguinaria_4", KitType.GUILD_SANGUINARIA_4);
        types.put("guilda_sanguinaria_5", KitType.GUILD_SANGUINARIA_5);
        types.put("guilda_nobre_1", KitType.GUILD_NOBRE_1);
        types.put("guilda_nobre_2", KitType.GUILD_NOBRE_2);
        types.put("guilda_nobre_3", KitType.GUILD_NOBRE_3);
        types.put("guilda_nobre_4", KitType.GUILD_NOBRE_4);
        types.put("guilda_nobre_5", KitType.GUILD_NOBRE_5);

        times.put("10minutos", KitTime.TEN_MINUTES);
        times.put("6horas", KitTime.SIX_PLAYED_HOURS);
        times.put("diario", KitTime.DAILY);
        times.put("semanal", KitTime.WEEKLY);
        times.put("20dias", KitTime.TWENTY_DAYS);
        times.put("mensal", KitTime.MONTHLY);


        names.put("basico", "&fItem básico");
        names.put("vip_conde", "&eItem Conde");
        names.put("vip_lord", "&bItem Lord");
        names.put("vip_duque", "&4Item Duque");
        names.put("vip_titan", "&5Item Titan");
        names.put("guilda_ancia_1", "&9Item Ancião");
        names.put("guilda_ancia_2", "&9Item Ancião");
        names.put("guilda_ancia_3", "&9Item Ancião");
        names.put("guilda_ancia_4", "&9Item Ancião");
        names.put("guilda_ancia_5", "&9Item Ancião");
        names.put("guilda_sanguinaria_1", "&cItem Sanguinário");
        names.put("guilda_sanguinaria_2", "&cItem Sanguinário");
        names.put("guilda_sanguinaria_3", "&cItem Sanguinário");
        names.put("guilda_sanguinaria_4", "&cItem Sanguinário");
        names.put("guilda_sanguinaria_5", "&cItem Sanguinário");
        names.put("guilda_nobre_1", "&2Item Nobre");
        names.put("guilda_nobre_2", "&2Item Nobre");
        names.put("guilda_nobre_3", "&2Item Nobre");
        names.put("guilda_nobre_4", "&2Item Nobre");
        names.put("guilda_nobre_5", "&2Item Nobre");
    }

    public FEKitAdd() {
        super("fekitadd");

        setPermissionMessage("&cSem permissão.");
        setPermission("enchant.admin");

        usage.addParameter(new Parameter("basico/guilda_(nome)_(nivel)/vip_(nome)", DefaultParameterType.STRING, true)); // 0
        usage.addParameter(new Parameter("10minutos/6horas/diario/semanal/20dias/mensal", DefaultParameterType.STRING, true)); // 1


        playerListener = (cmd) -> {
            var rawType = cmd.getArgument(0).toString().toLowerCase();
            val rawTime = cmd.getArgument(1).toString().toLowerCase();
            val player = cmd.getSender();

            if (!player.isOp())
                return false;

            if (!types.keySet().contains(rawType) && !rawType.startsWith("!"))
                return false;

            if (rawType.startsWith("!"))
                rawType = rawType.substring(1);

            if (!times.keySet().contains(rawTime))
                return false;

            val name = names.containsKey(rawType) ? rawType + "_" + rawTime : rawType;

            val displayName = names.containsKey(rawType) ? names.get(rawType).replace("${time}", MessageUtils.capitalize(rawTime)) : rawType;

            val invItems = Arrays.stream(player.getInventory().getContents())
                    .filter(itemStack -> itemStack != null && itemStack.getType() != null && itemStack.getType() != Material.AIR)
                    .map(item -> {
                        val meta = item.getItemMeta();
                        if (meta.hasLore() || meta.hasDisplayName())
                            return item;
                        meta.setLore(Collections.singletonList(ChatColor.translateAlternateColorCodes('&', displayName)));
                        item.setItemMeta(meta);
                        return item;
                    }).collect(Collectors.toList());

            Main.getInstance().getConfig().set("Kits." + name.toLowerCase() + ".Name", displayName);
            Main.getInstance().getConfig().set("Kits." + name.toLowerCase() + ".Type", types.containsKey(rawType) ? types.get(rawType).name() : "Special");
            Main.getInstance().getConfig().set("Kits." + name.toLowerCase() + ".Time", times.get(rawTime).name());
            Main.getInstance().getConfig().set("Kits." + name.toLowerCase() + ".Items", invItems);
            Main.getInstance().saveConfig();
            Main.getInstance().reloadConfig();

            cmd.reply("&aKit " + displayName + " &aadicionado com sucesso!");
            Main.getGui().open(player, new KitPreview(new MainMenu(), name.toLowerCase()));
            return true;
        };
    }
}
