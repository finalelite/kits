package br.com.finalelite.kits.commands;

import br.com.finalelite.pauloo27.api.commands.BaseCommand;
import br.com.finalelite.pauloo27.api.commands.usage.DefaultParameterType;
import br.com.finalelite.pauloo27.api.commands.usage.Parameter;
import lombok.val;
import org.bukkit.enchantments.Enchantment;

public class FEEnchantCommand extends BaseCommand {

    public FEEnchantCommand() {
        super("feenchant");

        setPermissionMessage("&cSem permissão.");
        setPermission("enchant.admin");

        usage.addParameter(new Parameter("nome do encantamento", DefaultParameterType.STRING, true));
        usage.addParameter(new Parameter("nível", DefaultParameterType.INTEGER, true));

        playerListener = (cmd) -> {
            val enchant = (String) cmd.getArgument(0);
            val level = (int) cmd.getArgument(1);
            val player = cmd.getSender();
            val item = player.getItemInHand();

            if (!player.isOp())
                return false;

            val ench = Enchantment.getByName(enchant.toUpperCase());
            if (ench == null)
                return false;

            item.addUnsafeEnchantment(ench, level);

            player.setItemInHand(item);

            return true;
        };
    }
}
