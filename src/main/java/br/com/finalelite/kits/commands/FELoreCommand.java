package br.com.finalelite.kits.commands;


import br.com.finalelite.pauloo27.api.commands.BaseCommand;
import br.com.finalelite.pauloo27.api.commands.usage.DefaultParameterType;
import br.com.finalelite.pauloo27.api.commands.usage.Parameter;
import lombok.val;
import lombok.var;
import org.bukkit.ChatColor;

import java.util.ArrayList;

public class FELoreCommand extends BaseCommand {

    public FELoreCommand() {
        super("felore");

        setPermissionMessage("&cSem permissão.");
        setPermission("enchant.admin");

        usage.addParameter(new Parameter("add ou clear", DefaultParameterType.STRING, true));
        usage.addParameter(new Parameter("texto", DefaultParameterType.STRING, false));

        playerListener = (cmd) -> {
            val action = (String) cmd.getArgument(0);
            val lore = cmd.getRawArgumentsBeforeAndIncluding(1);
            val player = cmd.getSender();
            val item = player.getItemInHand();
            val meta = item.getItemMeta();

            if (!player.isOp())
                return false;

            if (action.equalsIgnoreCase("clear"))
                meta.setLore(null);
            else if (action.equalsIgnoreCase("add")) {
                var l = meta.getLore();
                if (l == null)
                    l = new ArrayList<>();
                l.add(ChatColor.translateAlternateColorCodes('&', lore));
                meta.setLore(l);
            } else {
                return false;
            }

            item.setItemMeta(meta);
            player.setItemInHand(item);
            return true;
        };

    }
}
