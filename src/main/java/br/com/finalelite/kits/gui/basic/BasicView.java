package br.com.finalelite.kits.gui.basic;

import br.com.finalelite.kits.Main;
import br.com.finalelite.kits.gui.KitPreview;
import br.com.finalelite.kits.gui.MainMenu;
import br.com.finalelite.pauloo27.api.gui.BaseGUI;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class BasicView extends BaseGUI {

    @Override
    public Inventory buildInventory(Player player) {
        val inventory = Bukkit.createInventory(null, 9 * 3, ChatColor.YELLOW + "Kits Básicos");
        setItem(inventory, inventory.getSize() - 9, Material.ARROW, 1, "&cVoltar", null,
                (event) -> Main.getGui().update((Player) event.getWhoClicked(), new MainMenu()));

        setItem(inventory, 12, Material.WOODEN_PICKAXE, 1, "&eKit Inicial", null,
                event -> Main.getGui().update((Player) event.getWhoClicked(), new KitPreview(this, "basico_10minutos")));
        setItem(inventory, 14, Material.STONE_PICKAXE, 1, "&eKit 6 horas", null,
                event -> Main.getGui().update((Player) event.getWhoClicked(), new KitPreview(this, "basico_6horas")));

        return inventory;
    }

}
