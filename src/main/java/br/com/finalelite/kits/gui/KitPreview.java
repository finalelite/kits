package br.com.finalelite.kits.gui;

import br.com.finalelite.kits.Main;
import br.com.finalelite.kits.commands.KitsCommand;
import br.com.finalelite.pauloo27.api.gui.BaseGUI;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ItemFlag;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class KitPreview extends BaseGUI {

    private List<ItemStack> items;
    private BaseGUI parent;
    private String kitName;
    private String displayName;
    private int page;

    public KitPreview(BaseGUI parent, String kitName) {
        this(parent, kitName, 0);
    }

    public KitPreview(BaseGUI parent, String kitName, int page) {
        this.kitName = kitName;
        this.displayName = KitsCommand.getDisplayName(kitName);
        this.items = KitsCommand.getKitItems(kitName);
        this.page = page;
        this.parent = parent;
    }

    @Override
    public Inventory buildInventory(Player player) {
        if (displayName == null) {
            player.sendMessage(ChatColor.RED + "Kit inválido.");
            return Bukkit.createInventory(null, 9 * 6, ChatColor.translateAlternateColorCodes('&', "&cInválido"));
        }
        val inventory = Bukkit.createInventory(null, 9 * 6, ChatColor.translateAlternateColorCodes('&', displayName));
        return show(inventory, page);
    }

    private Inventory show(Inventory inventory, int page) {
        // TODO ???
        List<Integer> usable = Arrays.asList(
                10, 11, 12, 13, 14, 15, 16,
                19, 20, 21, 22, 23, 24, 25,
                28, 29, 30, 31, 32, 33, 34,
                37, 38, 39, 40, 41, 42, 43
        );

        inventory.clear();

        setItem(inventory, inventory.getSize() - 9, Material.ARROW, 1, "&cVoltar", null,
                (event) -> Main.getGui().update((Player) event.getWhoClicked(), parent));

        val collectItem = buildItem(Material.MUSIC_DISC_CHIRP, 1, "&aColetar", null);
        val collectMeta = collectItem.getItemMeta();
        collectMeta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
        collectItem.setItemMeta(collectMeta);

        setItem(inventory, 49, collectItem, event -> {
            ((Player) event.getWhoClicked()).chat("/kits " + kitName);
            Main.getGui().close((Player) event.getWhoClicked());
        });

        if (page > 0) {
            setItem(inventory, 0, Material.BLAZE_ROD, 1, "&aPágina anterior", null,
                    event -> show(inventory, page - 1));
        }

        val filteredItems = items.stream().skip(usable.size() * page).collect(Collectors.toList());
        if (filteredItems.size() > usable.size()) {
            setItem(inventory, 8, Material.BLAZE_ROD, 1, "&aPróxima página", null,
                    event -> show(inventory, page + 1));
        }

        AtomicInteger index = new AtomicInteger();
        filteredItems.stream().limit(usable.size()).forEach(itemStack -> inventory.setItem(usable.get(index.getAndIncrement()), itemStack));

        return inventory;
    }

}
