package br.com.finalelite.kits.gui.vip;

import br.com.finalelite.kits.Main;
import br.com.finalelite.kits.gui.MainMenu;
import br.com.finalelite.pauloo27.api.gui.BaseGUI;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class VipMenu extends BaseGUI {

    private static Map<String, Map.Entry<String, String>> vips = new LinkedHashMap<>();

    static {
        vips.put("Conde", new AbstractMap.SimpleEntry<>("e", "COAL"));
        vips.put("Lord", new AbstractMap.SimpleEntry<>("b", "GOLD"));
        vips.put("Duque", new AbstractMap.SimpleEntry<>("4", "IRON"));
        vips.put("Titan", new AbstractMap.SimpleEntry<>("5", "DIAMOND"));
    }

    @Override
        public Inventory buildInventory(Player player) {
        val inventory = Bukkit.createInventory(null, 9 * 3, ChatColor.YELLOW + "Kits VIP");

        setItem(inventory, inventory.getSize() - 9, Material.ARROW, 1, "&cVoltar", null,
                (event) -> Main.getGui().update((Player) event.getWhoClicked(), new MainMenu()));

        AtomicInteger slot = new AtomicInteger(10);
        vips.forEach((key, value) -> setItem(inventory, slot.getAndAdd(2), Material.valueOf(value.getValue() + "_BLOCK"), 1,
                "&eKits VIP &" + value.getKey() + "(" + key + ")", Arrays.asList("&7Adquira vantagens como esta em:", "&bloja.finalelite.com.br"),
                (event) -> Main.getGui().update((Player) event.getWhoClicked(), new VipView(key, value.getKey(), value.getValue()))));

        return inventory;
    }
}
