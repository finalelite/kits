package br.com.finalelite.kits.gui.vip;

import br.com.finalelite.kits.Main;
import br.com.finalelite.kits.gui.KitPreview;
import br.com.finalelite.pauloo27.api.gui.BaseGUI;
import lombok.AllArgsConstructor;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.Arrays;

@AllArgsConstructor
public class VipView extends BaseGUI {

    private final String name;
    private final String color;
    private final String oreName;

    @Override
    public Inventory buildInventory(Player playerToOpen) {
        val inventory = Bukkit.createInventory(null, 9 * 3, ChatColor.getByChar(color.charAt(0)) + "Kits " + name);

        setItem(inventory, inventory.getSize() - 9, Material.ARROW, 1, "&cVoltar", null,
                event -> Main.getGui().update((Player) event.getWhoClicked(), new VipMenu()));

        setItem(inventory, 10, Material.valueOf(oreName + "_BLOCK"), 1, "&" + color + "*Kit Mensal (" + name + ")", Arrays.asList("&7Clique para ver os itens ", "&7que este kit disponibiliza"),
                event -> {
                    val player = (Player) event.getWhoClicked();
                    Main.getGui().update(player, new KitPreview(this, "vip_" + name.toLowerCase() + "_mensal"));
                });


        setItem(inventory, 12, Material.valueOf(oreName + "_ORE"), 1, "&" + color + "*Kit Semanal (" + name + ")", Arrays.asList("&7Clique para ver os itens ", "&7que este kit disponibiliza"),
                event -> {
                    val player = (Player) event.getWhoClicked();
                    Main.getGui().update(player, new KitPreview(this, "vip_" + name.toLowerCase() + "_semanal"));
                });

        setItem(inventory, 14, Material.valueOf((oreName.equals("COAL") ? "STONE" : (oreName.equals("GOLD") ? "GOLDEN" : oreName)) + "_PICKAXE"),
                1, "&" + color + "*Kit Diário (" + name + ")", Arrays.asList("&7Clique para ver os itens ", "&7que este kit disponibiliza"),
                event -> {
                    val player = (Player) event.getWhoClicked();
                    Main.getGui().update(player, new KitPreview(this, "vip_" + name.toLowerCase() + "_diario"));
                });

        setItem(inventory, 16, Material.valueOf((oreName.equals("GOLD") ? "GOLD_INGOT" : oreName.equals("IRON") ? "IRON_INGOT" : oreName)),
                1, "&" + color + "*Kit 6 horas (" + name + ")", Arrays.asList("&7Clique para ver os itens ", "&7que este kit disponibiliza"),
                event -> {
                    val player = (Player) event.getWhoClicked();
                    Main.getGui().update(player, new KitPreview(this, "vip_" + name.toLowerCase() + "_6horas"));
                });


        return inventory;
    }
}
