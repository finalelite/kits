package br.com.finalelite.kits.gui.guild;

import br.com.finalelite.kits.Main;
import br.com.finalelite.kits.gui.KitPreview;
import br.com.finalelite.pauloo27.api.gui.BaseGUI;
import lombok.AllArgsConstructor;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.Arrays;

@AllArgsConstructor
public class GuildView extends BaseGUI {

    private final String name;
    private final String displayName;
    private final String color;

    @Override
    public Inventory buildInventory(Player player) {
        val inventory = Bukkit.createInventory(null, 9 * 3, ChatColor.getByChar(color.charAt(0)) + "Kits " + displayName);

        setItem(inventory, inventory.getSize() - 9, Material.ARROW, 1, "&cVoltar", null,
                (event) -> Main.getGui().update((Player) event.getWhoClicked(), new GuildMenu()));

        setItem(inventory, 9, Material.LEATHER_HELMET, 1, "&" + color + "Kit Nível 1", Arrays.asList("&7Clique para verificar os itens ", "&7que este kit disponibiliza"),
                (event) -> Main.getGui().update((Player) event.getWhoClicked(), new KitPreview(this, "guilda_" + name.toLowerCase() + "_1_20dias")));

        setItem(inventory, 11, Material.GOLDEN_HELMET, 2, "&" + color + "Kit Nível 2", Arrays.asList("&7Clique para verificar os itens ", "&7que este kit disponibiliza"),
                (event) -> Main.getGui().update((Player) event.getWhoClicked(), new KitPreview(this, "guilda_" + name.toLowerCase() + "_2_20dias")));

        setItem(inventory, 13, Material.CHAINMAIL_HELMET, 3, "&" + color + "Kit Nível 3", Arrays.asList("&7Clique para verificar os itens ", "&7que este kit disponibiliza"),
                (event) -> Main.getGui().update((Player) event.getWhoClicked(), new KitPreview(this, "guilda_" + name.toLowerCase() + "_3_20dias")));

        setItem(inventory, 15, Material.IRON_HELMET, 4, "&" + color + "Kit Nível 4", Arrays.asList("&7Clique para verificar os itens ", "&7que este kit disponibiliza"),
                (event) -> Main.getGui().update((Player) event.getWhoClicked(), new KitPreview(this, "guilda_" + name.toLowerCase() + "_4_20dias")));

        setItem(inventory, 17, Material.DIAMOND_HELMET, 5, "&" + color + "Kit Nível 5", Arrays.asList("&7Clique para verificar os itens ", "&7que este kit disponibiliza"),
                (event) -> Main.getGui().update((Player) event.getWhoClicked(), new KitPreview(this, "guilda_" + name.toLowerCase() + "_5_20dias")));

        return inventory;
    }
}
