package br.com.finalelite.kits.gui;

import br.com.finalelite.kits.Main;
import br.com.finalelite.kits.gui.basic.BasicView;
import br.com.finalelite.kits.gui.guild.GuildMenu;
import br.com.finalelite.kits.gui.vip.VipMenu;
import br.com.finalelite.pauloo27.api.gui.BaseGUI;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.Arrays;
import java.util.Collections;

public class MainMenu extends BaseGUI {

    @Override
    public Inventory buildInventory(Player player) {
        val inventory = Bukkit.createInventory(null, 9 * 3, ChatColor.YELLOW + "Kits");

        setItem(inventory, 10, Material.WOODEN_PICKAXE, 1, "&eKits básicos", Collections.singletonList("&7Kits para iniciar a sua jornada"),
                event -> Main.getGui().update((Player) event.getWhoClicked(), new BasicView()));
        setItem(inventory, 13, Material.DIAMOND_SWORD, 1, "&eKits VIPs", Arrays.asList("&7Adquira &bVIP &7e tenha vantagens", "&7como esta"),
                event -> Main.getGui().update((Player) event.getWhoClicked(), new VipMenu()));
        setItem(inventory, 16, Material.SHIELD, 1, "&eKits Guilda", Collections.singletonList("&7Kits da sua guilda"),
                event -> Main.getGui().update((Player) event.getWhoClicked(), new GuildMenu()));

        return inventory;
    }
}
