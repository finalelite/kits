package br.com.finalelite.kits.gui.guild;

import br.com.finalelite.kits.Main;
import br.com.finalelite.kits.gui.MainMenu;
import br.com.finalelite.pauloo27.api.gui.BaseGUI;
import lombok.val;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class GuildMenu extends BaseGUI {

    @Override
    public Inventory buildInventory(Player player) {
        val inventory = Bukkit.createInventory(null, 9 * 3, ChatColor.YELLOW + "Kits Guilda");

        setItem(inventory, inventory.getSize() - 9, Material.ARROW, 1, "&cVoltar", null,
                (event) -> Main.getGui().update((Player) event.getWhoClicked(), new MainMenu()));

        setItem(inventory, 10, Material.REDSTONE, 1, "&eKits Guilda &c(Sanguinária)", null,
                (event) -> Main.getGui().update((Player) event.getWhoClicked(), new GuildView("sanguinaria", "Sanguinária", "c")));
        setItem(inventory, 13, Material.BOOK, 1, "&eKits Guilda &9(Anciã)", null,
                (event) -> Main.getGui().update((Player) event.getWhoClicked(), new GuildView("ancia", "Anciã", "9")));
        setItem(inventory, 16, Material.EMERALD, 1, "&eKits Guilda &2(Nobre)", null,
                (event) -> Main.getGui().update((Player) event.getWhoClicked(), new GuildView("nobre", "Nobre", "2")));
        return inventory;
    }
}
